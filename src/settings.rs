// SPDX-License-Identifier: MIT License
use config::{Config, ConfigError, Environment, File};
use serde_derive::Deserialize;

#[derive(Debug, Default, Deserialize, Clone)]
pub enum Operation {
    #[default]
    And,
    Contains,
    Equals,
    Or,
}

#[derive(Debug, Default, Deserialize, Clone)]
pub struct Condition {
    pub key: String,
    pub operation: Operation,
    pub value: String,
    pub values: Vec<Condition>,
}

impl Condition {
    pub fn matches(&self, data: &serde_json::Value) -> bool {
        match &self.operation {
            Operation::Contains => {
                let v = &data[&self.key];
                match v {
                    serde_json::Value::Null => false,
                    _ => v.is_string() && v.as_str().unwrap().contains(&self.value),
                }
            }
            Operation::Equals => {
                let v = &data[&self.key];
                match v {
                    serde_json::Value::Null => false,
                    _ => v.is_string() && v.as_str().unwrap() == self.value,
                }
            }
            Operation::And => self
                .values
                .iter()
                .fold(true, |acc, v| acc && v.matches(data)),
            Operation::Or => self
                .values
                .iter()
                .fold(false, |acc, v| acc || v.matches(data)),
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct Function {
    pub rule: Condition,
    pub function: String,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Webhook {
    pub namespace: String,
    pub project: String,
    pub token: String,
    pub functions: Vec<Function>,
}

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub debug: bool,
    pub listen_address: String,
    pub port: u16,
    pub webhooks: Vec<Webhook>,
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let s = Config::builder()
            // Start off by merging in the "default" configuration file
            .add_source(File::with_name("Settings"))
            // Add in settings from the environment (with a prefix of APP)
            // Eg.. `APP_DEBUG=1 ./target/app` would set the `debug` key
            .add_source(Environment::with_prefix("app"))
            // You may also programmatically change settings
            .build()?;

        // You can deserialize (and thus freeze) the entire configuration as
        s.try_deserialize()
    }

    pub fn get(&self, namespace: String, project: String) -> Option<Webhook> {
        for webhook in self.webhooks.iter() {
            if namespace == webhook.namespace && project == webhook.project {
                return Some(webhook.clone());
            }
        }

        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json;

    macro_rules! operation_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
    #[test]
    fn $name() {
        let (condition, result) = $value;
        let json : serde_json::Value = serde_json::from_str(r#"{"name": "John Doe"}"#).unwrap();
        assert_eq!(condition.matches(&json), result);
    }
            )*
        }
    }

    macro_rules! mtrue {
        ($($x:expr),*) => {
            Condition {
                key: String::from("name"),
                operation: Operation::Equals,
                value: String::from("John Doe"),
                values: Vec::new(),
            }
        };
    }

    macro_rules! mfalse {
        ($($x:expr),*) => {
            Condition {
                key: String::from("name"),
                operation: Operation::Equals,
                value: String::from("JohnDoe"),
                values: Vec::new(),
            }
        };
    }

    operation_tests! {
        equals_ok: (Condition {
            key: String::from("name"),
            operation: Operation::Equals,
            value: String::from("John Doe"),
            values: Vec::new()
        }, true),
        equals_nok: (Condition {
            key: String::from("name"),
            operation: Operation::Equals,
            value: String::from("JohnDoe"),
            values: Vec::new()
        }, false),
        contains_ok: (Condition {
            key: String::from("name"),
            operation: Operation::Contains,
            value: String::from("hn D"),
            values: Vec::new()
        }, true),
        contains_nok: (Condition {
            key: String::from("name"),
            operation: Operation::Contains,
            value: String::from("hnD"),
            values: Vec::new()
        }, false),
        and_1_ok: (Condition {
            key: String::new(),
            operation: Operation::And,
            value: String::new(),
            values: [mtrue!()].to_vec(),
        }, true),
        and_1_nok: (Condition {
            key: String::new(),
            operation: Operation::And,
            value: String::new(),
            values: [mfalse!()].to_vec(),
        }, false),
        and_2_ok_ok: (Condition {
            key: String::new(),
            operation: Operation::And,
            value: String::new(),
            values: [mtrue!(), mtrue!()].to_vec(),
        }, true),
        and_2_nok_ok: (Condition {
            key: String::new(),
            operation: Operation::And,
            value: String::new(),
            values: [mfalse!(), mtrue!()].to_vec(),
        }, false),
        and_2_ok_nok: (Condition {
            key: String::new(),
            operation: Operation::And,
            value: String::new(),
            values: [mtrue!(), mfalse!()].to_vec(),
        }, false),
        and_2_nok_nok: (Condition {
            key: String::new(),
            operation: Operation::And,
            value: String::new(),
            values: [mfalse!(), mfalse!()].to_vec(),
        }, false),
        or_1_ok: (Condition {
            key: String::new(),
            operation: Operation::Or,
            value: String::new(),
            values: [mtrue!()].to_vec(),
        }, true),
        or_1_nok: (Condition {
            key: String::new(),
            operation: Operation::Or,
            value: String::new(),
            values: [mfalse!()].to_vec(),
        }, false),
        or_2_ok_ok: (Condition {
            key: String::new(),
            operation: Operation::Or,
            value: String::new(),
            values: [mtrue!(), mtrue!()].to_vec(),
        }, true),
        or_2_nok_ok: (Condition {
            key: String::new(),
            operation: Operation::Or,
            value: String::new(),
            values: [mfalse!(), mtrue!()].to_vec(),
        }, true),
        or_2_ok_nok: (Condition {
            key: String::new(),
            operation: Operation::Or,
            value: String::new(),
            values: [mtrue!(), mfalse!()].to_vec(),
        }, true),
        or_2_nok_nok: (Condition {
            key: String::new(),
            operation: Operation::Or,
            value: String::new(),
            values: [mfalse!(), mfalse!()].to_vec(),
        }, false),
    }
}
