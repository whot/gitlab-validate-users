// SPDX-License-Identifier: MIT License
mod server;
mod settings;

use crate::settings::Settings;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // unwrap may abort, but we are in the init phase
    let settings = Settings::new().unwrap();

    server::run(settings).await
}
